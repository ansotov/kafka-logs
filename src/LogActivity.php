<?php

namespace Logs;

use Illuminate\Support\Facades\Request;

class LogActivity
{
    /**
     * @param int         $code
     * @param string|null $message
     * @param string|null $trace
     *
     * @return array
     */
    public static function addToLog(int $code = 200, ?string $message = null, ?string $trace = null): array
    {
        $log            = [];
        $log['code']    = $code;
        $log['ms_type'] = 'library';
        $log['url']     = Request::fullUrl();
        $log['method']  = Request::method();
        $log['ip']      = Request::ip();
        $log['agent']   = Request::header('user-agent');
        $log['user_id'] = auth()->check() ? auth()->user()->id : null;
        $log['message'] = $message;
        $log['trace']   = $trace;

        return $log;
    }
}
