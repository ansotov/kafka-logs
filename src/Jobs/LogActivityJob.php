<?php

namespace Logs\Jobs;

class LogActivityJob extends Job
{
    public function __construct(
        public readonly array $data
    ) {
    }
}
