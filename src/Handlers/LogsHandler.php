<?php

namespace Logs\Handlers;

use Logs\Handlers\Contracts\LogsHandlerInterface;
use Logs\LogActivity;
use App\Jobs\LogActivityJob;
use Throwable;

class LogsHandler implements LogsHandlerInterface
{
    /**
     * @param \Throwable $exception
     *
     * @return void
     */
    public function handle(Throwable $exception): void
    {
        /**
         * Log data
         */
        $code = method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : 500;

        LogActivityJob::dispatch(
            LogActivity::addToLog(
                $code,
                $exception->getMessage(),
                $exception->getTraceAsString()
            )
        )->onQueue(config('app.'));
    }
}
