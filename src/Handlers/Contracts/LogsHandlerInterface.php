<?php

namespace Logs\Handlers\Contracts;

use Throwable;

interface LogsHandlerInterface
{
    public function handle(Throwable $exception): void;
}
